#include "cube.h"

int main() {
  assert(!setvbuf(stdout, stdout_buffer, _IOFBF, STDOUT_BUFFER_SIZE));
  while (true) {
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    if (w.ws_row != old_row_size || w.ws_col != old_col_size) {
      old_row_size = w.ws_row;
      old_col_size = w.ws_col;
      free(buffer);
      free(zBuffer);
      buffer = malloc(w.ws_row * w.ws_col * sizeof(char));
      zBuffer = malloc(w.ws_row * w.ws_col * sizeof(float));
    }
    memset(buffer, BACKGROUND, w.ws_col * w.ws_row);
    memset(zBuffer, 0, w.ws_col * w.ws_row * sizeof(float));
    for (v1 = -v3; v1 < v3; v1 += incrementDelta) {
      for (v2 = -v3; v2 < v3; v2 += incrementDelta) {
        createFace(v1, v2, -v3, '@');
        createFace(v1, v2, v3, '%');
        createFace(-v3, v1, v2, '$');
        createFace(v3, v1, v2, '#');
        createFace(v1, -v3, v2, '+');
        createFace(v1, v3, v2, '=');
      }
    }
    renderFrameWithColor();
    fflush(stdout);
    xTheta += 0.005;
    yTheta += 0.005;
    zTheta += 0.000;
  }
  write(STDOUT_FILENO, "\x1b[1;1H\x1b[3J", 10);
  free(zBuffer);
  return 0;
}

float rotateX(int x, int y, int z) {
  return y * sin(xTheta) * sin(yTheta) * cos(zTheta) -
         z * cos(xTheta) * sin(yTheta) * cos(zTheta) +
         y * cos(xTheta) * sin(zTheta) + z * sin(xTheta) * sin(zTheta) +
         x * cos(yTheta) * cos(zTheta);
}

float rotateY(int x, int y, int z) {
  return y * cos(xTheta) * cos(zTheta) + z * sin(xTheta) * cos(zTheta) -
         y * sin(xTheta) * sin(yTheta) * sin(zTheta) +
         z * cos(xTheta) * sin(yTheta) * sin(zTheta) -
         x * cos(yTheta) * sin(zTheta);
}

float rotateZ(int x, int y, int z) {
  return z * cos(xTheta) * cos(yTheta) - y * sin(xTheta) * cos(yTheta) +
         x * sin(yTheta);
}

void createFace(float x, float y, float z, char stamp) {
  xr = rotateX(x, y, z);
  yr = rotateY(x, y, z);
  zr = rotateZ(x, y, z) + distanceFromCamera;
  zInverse = 1 / zr;
  xp = (int)(w.ws_col / 2 + k * xr * zInverse * 2);
  yp = (int)(w.ws_row / 2 + k * yr * zInverse);
  idx = xp + yp * w.ws_col;
  if (idx >= 0 && idx < (w.ws_col * w.ws_row)) {
    if (*(zBuffer + idx) < zInverse) {
      *(zBuffer + idx) = zInverse;
      *(buffer + idx) = stamp;
    }
  }
}

void renderFrameWithoutColor() {
  write(STDOUT_FILENO, "\x1b[3J\x1b[1;1H", 10);
  for (i = 0; i < old_row_size; ++i) {
    printf("%.*s\n", old_col_size, buffer + i * old_col_size);
  }
  fflush(stdout);
}

void renderFrameWithColor() {
  write(STDOUT_FILENO, "\x1b[3J\x1b[1;1H", 10);
  for (x = 0; x < old_row_size; ++x) {
    for (y = 0; y < old_col_size; ++y) {
      i = x * old_col_size + y;
      switch (buffer[i]) {
      case '@':
        printf("\x1b[31m%c\x1b[0m", buffer[i]);
        break;
      case '%':
        printf("\x1b[32m%c\x1b[0m", buffer[i]);
        break;
      case '$':
        printf("\x1b[33m%c\x1b[0m", buffer[i]);
        break;
      case '#':
        printf("\x1b[34m%c\x1b[0m", buffer[i]);
        break;
      case '+':
        printf("\x1b[35m%c\x1b[0m", buffer[i]);
        break;
      case '=':
        printf("\x1b[36m%c\x1b[0m", buffer[i]);
        break;
      case ' ':
      default:
        printf("%c", buffer[i]);
        break;
      }
    }
  }
  fflush(stdout);
}
