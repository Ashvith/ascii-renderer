#ifndef CUBE_H
#define CUBE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <sys/ioctl.h>
#include <limits.h>
#include <assert.h>
#include <stdbool.h>

#define STDOUT_BUFFER_SIZE (256 * 256)
#define BACKGROUND ' '

char stdout_buffer[STDOUT_BUFFER_SIZE];
float xTheta, yTheta, zTheta;
int distanceFromCamera = 200;
char *buffer;
float *zBuffer;
int x, y, i;
float xr, yr, zr, zInverse;
int k = 100, xp, yp, idx;
struct winsize w;
unsigned short old_row_size = SHRT_MIN, old_col_size = SHRT_MIN;
float v1, v2, v3 = 25, incrementDelta = 1;

float rotateX(int x, int y, int z);
float rotateY(int x, int y, int z);
float rotateZ(int x, int y, int z);
void createFace(float x, float y, float z, char stamp);
void renderFrameWithoutColor();
void renderFrameWithColor();

#endif
