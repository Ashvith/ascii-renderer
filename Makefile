CC=gcc
CFLAGS=-std=c17 -Wall -Wextra -pedantic -mssse3 -mpopcnt -mtune=skylake -O5 -lm -I$(IDIR)

IDIR=./include/
SRCDIR=./src/

SOURCES=${SRCDIR}*.c

all: cube run clean

cube:
	$(CC) $(SOURCES) $(CFLAGS) -o $@

run:
	./cube

clean:
	rm cube
